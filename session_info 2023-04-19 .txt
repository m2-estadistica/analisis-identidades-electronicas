R version 4.2.1 (2022-06-23 ucrt)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 19044), RStudio 2022.7.2.576

Random number generation:
 RNG:     Mersenne-Twister 
 Normal:  Inversion 
 Sample:  Rounding 
 
Locale:
  LC_COLLATE=Spanish_Spain.utf8  LC_CTYPE=Spanish_Spain.utf8    LC_MONETARY=Spanish_Spain.utf8
  LC_NUMERIC=C                   LC_TIME=Spanish_Spain.utf8    

Package version:
  backports_1.4.1     base64enc_0.1-3     bslib_0.4.2         cachem_1.0.7        checkmate_2.1.0    
  cli_3.6.1           codetools_0.2-18    commonmark_1.9.0    compiler_4.2.1      cpp11_0.4.3        
  crayon_1.5.2        curl_5.0.0          digest_0.6.31       dplyr_1.1.1         ellipsis_0.3.2     
  fansi_1.0.4         fastmap_1.1.1       fontawesome_0.5.0   fs_1.6.1            generics_0.1.3     
  glue_1.6.2          graphics_4.2.1      grDevices_4.2.1     hms_1.1.3           htmltools_0.5.5    
  httpuv_1.6.9        janitor_2.2.0       jquerylib_0.1.4     jsonlite_1.8.4      later_1.3.0        
  lifecycle_1.0.3     lobstr_1.1.2        lubridate_1.9.2     magick_2.7.4        magrittr_2.0.3     
  MASS_7.3-57         matrixStats_0.63.0  memoise_2.0.1       methods_4.2.1       mime_0.12          
  nortest_1.0-4       pacman_0.5.1        pander_0.6.5        pillar_1.9.0        pkgconfig_2.0.3    
  plyr_1.8.8          prettyunits_1.1.1   promises_1.2.0.1    pryr_0.1.6          purrr_1.0.1        
  R6_2.5.1            rappdirs_0.3.3      rapportools_1.1     Rcpp_1.0.10         remotes_2.4.2      
  reshape2_1.4.4      rlang_1.1.0         rstudioapi_0.14     sass_0.4.5          shiny_1.7.4        
  snakecase_0.11.0    sourcetools_0.1.7.1 stats_4.2.1         stringi_1.7.12      stringr_1.5.0      
  summarytools_1.0.1  tcltk_4.2.1         tibble_3.2.1        tidyr_1.3.0         tidyselect_1.2.0   
  timechange_0.2.0    tools_4.2.1         utf8_1.2.3          utils_4.2.1         vctrs_0.6.1        
  withr_2.5.0         xfun_0.38           xtable_1.8-4       
