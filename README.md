# Análisis de Identidades Electrónicas


## Proyecto Final del Módulo: Tópicos de Estadística Orientada a la Investigación
Se desarrolló con el propósito de realizar un análisis de las cantidades de identidades creadas en Paraguay, mediante fuentes de información oficiales del país.

La fuente de datos refleja las cantidades de identidades electrónicas creadas por mes, año y discriminadas por sexo:
- Se utilizaron fuentes de información oficiales de Paraguay: [Datos Abiertos](https://www.datos.gov.py)
- Previo al estudio estadístico, los datos fueron organizados y consolidados
- Se utilizó la Prueba de Bondad de Ajuste para verificar qué distribución siguen los datos analizados y consecuentemente, qué pruebas llevar a cabo en el contraste estadístico.
- Mediante la Regresión Lineal Simple se analizó la correlación entre la cantidad total de identidades creadas por mes y la cantidad de identidades creadas por sexo en un mes.
- Los datos se procesaron, analizaron y graficaron con el software estadístico R.