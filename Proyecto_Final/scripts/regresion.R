# Titulo: Análisis de Identidades Elecrónicas creadas en los últimos 3 años

# Propósito: Este script crea la estructura para  realizar pruebas de regresión.

# Autores: Daniel Rojas - Liliana Simón - Marcos Centurión
# e-mails: danyrojassimon@fpuna.edu.py - lilianasimon@fpuna.edu.py - marcos.centurion@fpuna.edu.py

Sys.time()

# Fecha de implementación del script: "2023-04-19 18:00:00 -04" ----
# Fecha de ultima modificación: "2023-05-02 21:23:55 -04" ----


# package dependencies
library(ggplot2)


# Asignar un directorio de trabajo
rstudioapi::getSourceEditorContext()$path
dirname(rstudioapi::getSourceEditorContext()$path)
setwd(dirname(rstudioapi::getSourceEditorContext()$path))

#Script

#La Regresión Lineal Simple es un modelo de regresión en donde una función lineal representa la relación existente entre una variable dependiente y su respectiva variable independiente.

#Leemos los archivos donde se encuentran los datos de identidades electrónicas creadas desde el 2016 hasta el 2022.
#Usaremos los datos consolidados como conjunto de datos de entrenamiento.
train <- read.csv(file="../data/raw_data/cantidad-identidad-electronica-consolidado.csv", header = TRUE, sep =';')
#Usaremos los datos de los últimos tres años como conjunto de datos de validación.
test <- read.csv(file="../data/raw_data/cantidad-identidad-electronica.csv", header = TRUE, sep =',')
#Usaremos el conjunto de entrenamiento para entrenar nuestro modelo de regresión, y luego se empleará el conjunto de validación para probar la calidad del modelo construido.

#Primera parte:
#Variable dependiente: total_mes
#Variable independiente: cantidad_hombres

#Graficamos el conjunto de entrenamiento.
#Aplicar una regresión sobre estos datos implica obtener la línea recta que mejor ajuste la relación existente entre la variable independiente y la variable dependiente.
ggplot() + geom_point(data = train, aes(x = cantidad_hombres, y = total_mes)) + 
  xlab("Variable Independiente") + 
  ylab("Variable Dependiente") + 
  ggtitle("Conjunto de Entrenamiento")


#Vamos a crear un regresor haciendo uso de la función lm del paquete stats
set.seed(1234)

#Como primer argumento de lm se coloca la fórmula variable dependiente ~ variable independiente.
#Como segundo argumento se indica el conjunto de datos que se usará para construir el modelo.
regresor <- lm(total_mes ~ cantidad_hombres, data = train)
summary(regresor)

#Creamos un un vector de predicciones basado en el propio conjunto de entrenamiento, y con este vector podremos visualizar la curva de ajuste de los datos.
y_predict <- predict(regresor, train)
#Construimos la gráfica adecuada.
ggplot() + geom_point(data = train, aes(x = cantidad_hombres, y = total_mes), size = 0.9) +
  geom_line(aes( x = train$cantidad_hombres, y = y_predict), color = "red") +
  xlab("Variable Independiente") + 
  ylab("Variable Dependiente") + 
  ggtitle("Curva de Ajuste sobre Conjunto de Entrenamiento")

#Ya que contamos con el regresor, vamos a realizar predicciones sobre el conjunto de validación.
y_test_predict <- predict(regresor, test)
#Construimos la gráfica adecuada.
ggplot() + geom_point(data = test, aes(x = cantidad_hombres, y = total_mes), size = 0.9) + 
  geom_line(aes( x = test$cantidad_hombres, y = y_test_predict), color = "red") +
  xlab("Variable Independiente") + 
  ylab("Variable Dependiente") + 
  ggtitle("Curva de Ajuste sobre Conjunto de Validación")

#Observamos la correlación existente entre los valores y del conjunto de entrenamiento, y los valores predichos para dicho conjunto.
cor(test$total_mes, y_test_predict)

#Teniendo nuestro modelo de regresor listo, podemos realizar una predicción particular para cualquier valor de x.
predict_value <- predict(regresor, data.frame(cantidad_hombres = c(2500)))
predict_value

#Segunda parte:
#Variable dependiente: total_mes
#Variable independiente: cantidad_mujeres

#Graficamos el conjunto de entrenamiento.
#Aplicar una regresión sobre estos datos implica obtener la línea recta que mejor ajuste la relación existente entre la variable independiente y la variable dependiente.
ggplot() + geom_point(data = train, aes(x = cantidad_mujeres, y = total_mes)) + 
  xlab("Variable Independiente") + 
  ylab("Variable Dependiente") + 
  ggtitle("Conjunto de Entrenamiento")


#Vamos a crear un regresor haciendo uso de la función lm del paquete stats
set.seed(1234)

#Como primer argumento de lm se coloca la fórmula variable dependiente ~ variable independiente.
#Como segundo argumento se indica el conjunto de datos que se usará para construir el modelo.
regresor <- lm(total_mes ~ cantidad_mujeres, data = train)
summary(regresor)

#Creamos un un vector de predicciones basado en el propio conjunto de entrenamiento, y con este vector podremos visualizar la curva de ajuste de los datos.
y_predict <- predict(regresor, train)
#Construimos la gráfica adecuada.
ggplot() + geom_point(data = train, aes(x = cantidad_mujeres, y = total_mes), size = 0.9) +
  geom_line(aes( x = train$cantidad_mujeres, y = y_predict), color = "red") +
  xlab("Variable Independiente") + 
  ylab("Variable Dependiente") + 
  ggtitle("Curva de Ajuste sobre Conjunto de Entrenamiento")

#Ya que contamos con el regresor, vamos a realizar predicciones sobre el conjunto de validación.
y_test_predict <- predict(regresor, test)
#Construimos la gráfica adecuada.
ggplot() + geom_point(data = test, aes(x = cantidad_mujeres, y = total_mes), size = 0.9) + 
  geom_line(aes( x = test$cantidad_mujeres, y = y_test_predict), color = "red") +
  xlab("Variable Independiente") + 
  ylab("Variable Dependiente") + 
  ggtitle("Curva de Ajuste sobre Conjunto de Validación")

#Observamos la correlación existente entre los valores y del conjunto de entrenamiento, y los valores predichos para dicho conjunto.
cor(test$total_mes, y_test_predict)

#Teniendo nuestro modelo de regresor listo, podemos realizar una predicción particular para cualquier valor de x.
predict_value <- predict(regresor, data.frame(cantidad_mujeres = c(2500)))
predict_value